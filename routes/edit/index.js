const user = require ('./../../scheme/users')
const log = require ('./../../libs/logs')(module);
module.exports=function (req, res){
    user.findById(req.body.id).exec((err,result)=>{
        result.set({num:req.body.num});
        result.save((err)=>{
            if(err){
                log.error(err)
                res.send("Что-то не так");
            }
            else
                res.send("Все хорошо, значение успешно изменено");
        })
    })
}