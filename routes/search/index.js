const users = require('../../scheme/users');
const log = require('./../../libs/logs')(module);

module.exports=function (req, res) {
  users.find({ip:{$regex:req.body.ip}}).exec((err, result) => {
    if (err) {
        log.error(err)
        res.send ([])
    }else
        if(result===null){
          res.send ([])
        }else{
          res.send (result)
        }
  });
}